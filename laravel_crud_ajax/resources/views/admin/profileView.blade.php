@extends('admin.layout.master')
@section('header','Profile')
@section('content')

<div class="profile-view">
	<center>
	<img src="img/download.png" alt="">
	<br><br>
	<table class="card w-50">
		<tbody>
			<tr>
				<td><b>First Name</b></td>
				<td><b>:</b></td>
				<td>{{ $user->name }}</td>
			</tr>
			<tr>
				<td><b>Email</b></td>
				<td><b>:</b></td>
				<td>{{ $user->email }}</td>
			</tr>
		</tbody>
	</table>
</center>
</div>
@endsection
@extends('admin.layout.master')
@section('header','Dashboard')
@section('title','Dashboard')
@section('content')
@php
$genderArray = config('constant.gender');
@endphp
<?php //echo "<pre>";print_R(config('constant.gender'));?> 

  <!-- The Modal -->
        <div class="row pt-5 mt-5">
        
        <style>
        .collapsible {
          background-color: #777;
          color: white;
          cursor: pointer;
          margin-left: 15px;
          padding: 18px;
          width: 97%;
          margin-bottom: 15px;
          border: none;
          text-align: left;
          outline: none;
          font-size: 15px;
        }


        .collapsible:after {
          content: '\002B';
          color: white;
          font-weight: bold;
          float: right;
          margin-left: 5px;
        }

        .active:after {
          content: "\2212";
        }

        .content {
          padding: 0 18px;
          max-height: 0;
          overflow: hidden;
          transition: max-height 0.2s ease-out;
          background-color: #f1f1f1;
        }
        </style>

        <body>
        
        <button class="collapsible">Open Collapsible</button>
        <div class="content container" style="background-color:white;margin-bottom:10px;width: 97%;" >
          <form action="{{url('/dashboard')}}" method="get" name="userSearchForm">
            <div class="row">
             
            <div class="form-group col-3">
                <h6>Name</h6>
                 <select style="padding-left: 50px;padding-right: 50px;padding-top: 2px;padding-bottom: 2px;" name = "dropdown">
                       <option value = "" selected>Name</option>
                 
                      @if(count($getAllUser)>0)                 
                      @foreach($getAllUser as $user)
                       <option value="{{ $user->id}}" {{isset($request->dropdown) && $request->dropdown ==  $user->id?'selected':''}} >{{ $user->name.' '.$user->lastname}}</option>
                      @endforeach
                      @endif
                   </select>

            </div>
            <div class="form-group col-3">

                <h6>Email</h6>


                <input type="email" id="email" name="email" value="{{isset($request->email) ?$request->email:''}}" />


            </div>
            <div class="form-group col-3">
            
                <h6>Number</h6>
                <input type="number" id="number" name="number" value="{{isset($request->number) ?$request->number:''}}" />

            
             </div>

            <div class="form-group col-3">
                 
                <h6>Gender</h6>
                  <select style="padding-left: 50px;padding-right: 50px;padding-top: 2px;padding-bottom: 2px;" name = "gender">
                          <option value = "" selected>Select</option>
                          @foreach($genderArray as $key => $val)
                       <option value="{{ $key}}" {{isset($request->gender) && $request->gender ==  $key?'selected':''}}>{{ $val}}</option>
                      @endforeach

                  </select>
             </div>

            <div>
              <button style="margin-bottom: 20px;padding-left: 15px;padding-right: 15px;margin-left:300px;margin-right: 100px;" type="submit" value="1">Search</button>
  

              <input type="reset" value="Reset" />
          
             
            </div>

          </div>
        </form>
        </div>

        <script>
        var coll = document.getElementsByClassName("collapsible");
        var i;

        for (i = 0; i < coll.length; i++) {
          coll[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var content = this.nextElementSibling;
            if (content.style.maxHeight){
              content.style.maxHeight = null;
            } else {
              content.style.maxHeight = content.scrollHeight + "px";
            } 
          });
        }
        </script>


          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h5 class="card-category">All Persons List</h5>
                <h4 class="card-title"> User Stats</h4>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table">
                      <thead class=" text-primary">
                            <tr>
                            <th>profile_Image</th>
                            <th>@sortablelink('name')</th> 
                            <th>@sortablelink('lastname')</th> 
                            <th>@sortablelink('address')</th> 
                            <th>zipcode</th> 
                            <th>mobileNumber</th>  
                            <th>@sortablelink('email')</th>  
                            <th>gender</th> 
                            <th>status</th> 
                            <th>action</th> 
                          </tr>
                      </thead>
                       @if($record->count())
                         @foreach($record as $res)
                          <tr>
                            <th><img src="img/{{$res->profile_Image}}" /></th>
                            <th>{{$res->name }}</th> 
                            <th>{{$res->lastname}}</th> 
                            <th>{{$res->address}}</th> 
                            <th>{{$res->zipcode}}</th> 
                            <th>{{$res->mobileNumber}}</th>  
                            <th>{{$res->email}}</th>  
                            <th>{{$res->gender}}</th> 
                            <th><a href="/update_user_status/{{$res->id}}/{{$res->status}}" class="btn btn-primary">
                                  @if($res->status==1)
                                    deactive
                                  @else
                                    active
                                  @endif

                            </th> 
                            <th><a href="/update_user/{{$res->id}}"><i class="fa fa-edit"></i></a><a href="/delete_user/{{$res->id}}"><i class=" fa fa-trash"></i></a></th>
                           
                          </tr>
                          @endforeach
                         
                          @endif
                     
                  </table>

                </div>
              </div>
            </div>
                       {!! $record->appends(request()->except('index'))->render() !!}
               <!-- 1st part -->
<!-- The Modal -->
          </div>
        </div>
  <div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Modal Heading</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          {{ Form::open(array('url' => url('/update_user'),'files' => true ,'id'=>'upload_userdata')) }}
            @csrf  
            {!! Form::hidden('id','', array('class' => 'form-control' , 'id' => 'id')) !!}                          
            <center>   
            <img src="" class="img-thumbnail" alt="" id="image"> <br><br></center>
            {!! Form::label('name', '' , array('class'=>'form-lable')) !!}
            {!! Form::text('name','' , array('class' => 'form-control' , 'id' => 'name')) !!}
            <p class="text-danger name"></p>
            <br>

            {!! Form::label('Last Name', '' , array('class'=>'form-lable')) !!}
            {!! Form::text('lastName','', array('class' => 'form-control' , 'id' => 'lastName')) !!}
            <p class="text-danger lastName"></p>  

            <br>
            {!! Form::label('Address', '' , array('class'=>'form-lable')) !!}
            {!! Form::text('address','',array('class' => 'form-control' , 'id' => 'address')) !!}
            <p class="text-danger address"></p>
            <br>

            {!! Form::label('Zipcode', '' , array('class'=>'form-lable')) !!}
            {!! Form::number('zipcode','',array('class' => 'form-control' , 'id' => 'zipcode')) !!}
            <p class="text-danger zipcode"></p>
            <br>

            {!! Form::label('Mobile Number', '' , array('class'=>'form-lable')) !!}
            {!! Form::number('mobileNumber','',array('class' => 'form-control' , 'id' => 'mobileNumber')) !!}
            <p class="text-danger mobileNumber"></p>
            <br>
            
            {!! Form::label('Email', '' , array('class'=>'form-lable')) !!}
            {!! Form::email('email', '' ,array('class' => 'form-control' , 'id' => 'email')) !!}
            <p class="text-danger email"></p>
            <br>


            {!! Form::label('Gender', '' , array('class'=>'form-lable')) !!} &nbsp;&nbsp;
            {{ Form::radio('gender', 'male',null ,['id'=>'male'] ) }}
            {!! Form::label('Male' ,'' , array('class'=>'form-lable')) !!} &nbsp;&nbsp;
            {{ Form::radio('gender', 'female',null ,['id'=>'female']) }}
            {!! Form::label('Female' ,'', array('class'=>'form-lable')) !!}
            
            <br> <br>
            
            {!! Form::label('Profile Image', '' , array('class'=>'form-lable')) !!} <br>
            {!! Form::file('image',array('class' => 'form-control' , 'id' => 'image')) !!}
          </div>
            <center> {!! Form::submit('submit',array('class'=>'btn-submit'  , 'id' => 'submit')); !!}</center>
        
        <!-- Modal footer -->
        <div class="modal-footer">
           {!! Form::close() !!}
        </div>
        
      </div>
    </div>
  </div>  

      <script>
$(document).ready(function(){
  $("#searchData").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#tbody tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
@endsection

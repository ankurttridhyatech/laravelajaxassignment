@extends('admin.layout.master')
@section('header','Add state')
@section('title','Add state')
@section('content')

		{{ Form::open(array('url' => url('/add_state_data'),'files' => true, 'class'=>'card mt-5 w-100 ','id'=>'state_form')) }}
	<h1 class="font-color" style="text-align: center">ADD STATE</h1>
			@csrf


		<table>
      <th>
        <form>
                    
                <div class="form-group">
                <label for="state">State:</label>
                <input type="name" id="name" name="stateName"><br><br>
                <p class="text-danger clean name"></p> 
                 <span class="text-danger">{{ $errors->first('stateName') }}</span>
               </div>
              <input type="submit" class="btn btn-primary ml-5" value="ADD" >
        </form>
      </th>
    </table>
<table class="table table-striped">
  <thead>
    <tr>
       <th scope="col">ID</th>
      <th scope="col">Name</th>
      <th scope="col">Action</th>
    </tr>
  </thead>



     @foreach($users as $res)
        <tr>
          <th>{{ $res->id }}</th>
           <th>{{$res->name }}</th>
           <th><a href="/update_state/{{$res->id}}" data-toggle="modal" data-target="#myModal" ><i class="fa fa-edit"></i></a><a href="/delete_state/{{$res->id}}"><i class=" fa fa-trash"></i></a></th>
        </tr>
     @endforeach
</table>
{{ $users->links() }}
       
      <div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Update State</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          {{ Form::open(array('url' => url('/update_state/{id}'),'files' => true,'id'=>'updatestate'))}}
            @csrf  
            {!! Form::hidden('id','', array('class' => 'form-control' , 'id' => 'id')) !!}                          
            
          <div class="form-group">
                <label for="state">State:</label>
                <input type="name" id="name1" name="stateName1"><br><br>
                <p class="text-danger clean name"></p> 
                 <span class="text-danger">{{ $errors->first('stateName1') }}</span>
               </div>
            <br>

            <center> {!! Form::submit('submit',array('class'=>'btn-submit'  , 'id' => 'submit')); !!}</center>
        
        <!-- Modal footer -->
        <div class="modal-footer">
           {!! Form::close() !!}
        </div>
        
      </div>
    </div>
  </div>  

     
	


@endsection
			

		      
            	

			


